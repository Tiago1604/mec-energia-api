import pytest
from utils.subgroup_util import Subgroup

class TestSubgrupoInfo:

    def test_get_subgroup_info_existing(self):
        subgroup_name = 'A3'
        
        subgroup_info = Subgroup.get_subgroup_info(subgroup_name)
        
        assert subgroup_info['name'] == subgroup_name 
        

    def test_get_subgroup_null(self):
        
        subgroup_name = ''
        
        with pytest.raises(ValueError, match=f'subgroup name can not be empty'):
            Subgroup.get_subgroup_info(subgroup_name)


    def test_get_subgroup_info_invalid_name(self):
        subgroup_name = 'M3'
        
        with pytest.raises(ValueError, match=f'Invalid subgroup name: {subgroup_name}'):
            Subgroup.get_subgroup_info(subgroup_name)



        
    
