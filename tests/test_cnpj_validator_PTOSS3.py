import pytest

from utils.cnpj_validator_util import CnpjValidator

def test_invalid_cnpj_all_zeros():
    with pytest.raises(Exception, match='Invalid CNPJ: CNPJ cannot be all zeros'):
        CnpjValidator.validate("00000000000000")

def test_invalid_cnpj_non_numeric():
    with pytest.raises(Exception, match='CNPJ must contain exactly 14 numerical digits'):
        CnpjValidator.validate("585771A4000189")

def test_invalid_cnpj_length():
    with pytest.raises(Exception, match='CNPJ must contain exactly 14 numerical digits'):
        CnpjValidator.validate("5857714000189")

def test_invalid_cnpj_digit():
    with pytest.raises(Exception, match='CNPJ must contain exactly 14 numerical digits'):
        CnpjValidator.validate("58577B4000189")

def test_invalid_cnpj_length_digit():
    with pytest.raises(Exception, match='CNPJ must contain exactly 14 numerical digits'):
        CnpjValidator.validate("585771140001894")

def test_invalid_cnpj_verification_digit():
    with pytest.raises(Exception, match='Invalid CNPJ'):
        CnpjValidator.validate("58577114000184")

def test_valid_cnpj():
    CnpjValidator.validate("00038174000143")